n=1;
precision=input('Level of precision: ');% a user-defined level of precision
pi4=1/n;
delta=abs(pi-pi4*4);
while delta>precision
    n=n+1;
    pi4=pi4+(-1)^(n-1)*1/(2*n-1);
    delta=abs(pi-pi4*4);
end
%Define x y which have n rows and 1 column and plot them into the figure
x=rand(n,1);
y=rand(n,1);
figure('color','white');
hold all
axis square;
x1=x-0.5;
y1=y-0.5; %cirle has centre at (0.5,0.5)
r=x1.^2+y1.^2;
m=0;   %Number of points inside circle
for i=1:n
    if r(i)<=0.25
        m=m+1;
        plot(x(i),y(i),'b.');%use blue to plot the point inside the circle
    else
        plot(x(i),y(i),'r.') ;%use red to plot the point outside the circle
    end
end
theta=0:0.01:2*pi; %set of angles for the circle
circlex=0.5+0.5*cos(theta); %function to create the x values of the circle.
%circle is centered at (0.5,0.5) and has a radius of 0.5
circley=0.5+0.5*sin(theta); %function to create y values of circle
plot(circlex,circley,'r','linewidth',1.5); hold off; %plot the circle inside of the square. each side of the square should be tangential to the edge of the circle

%display the estimated value of pi in this precision
s2=sprintf('the estimated value of pi is %f', pi4*4);
disp(s2)