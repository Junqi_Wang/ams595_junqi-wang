n=1;
pi4=1/n;
delta=abs(pi-pi4*4);
precision=0.001;%The level of precision which I set here.
tic
while delta>precision
    n=n+1;
    pi4=pi4+(-1)^(n-1)*1/(2*n-1);
    delta=abs(pi-pi4*4);
end
s1=sprintf('the number of steps is %f when the the level of precision reaches %d',n,precision);%display the total steps in this level of precision
s2=sprintf('the estimated value of pi is %f', pi4*4);%display the estimated value of pi in this precision
disp(s1),disp(s2)
h=toc;
s3=sprintf('the time cost using while is %f second',h);%display the time cost 
disp(s3)