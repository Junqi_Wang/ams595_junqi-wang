pi_ref=3.141592; %this is pi to 6 decimal places. I'm going to use this as
%my 'accepted' value of pi so that I can make error calculations with what                                                                                                                                                          
%the algorithm spits out and see how it changes as the number of randomly
%generated points increases.

%using up to 100 points to estimate
numpoint1=100; %this is the total number of points we want to generate.
%these points are going to fall either inside of the circle or inside of
%the square. we'll identify the ones that fell inside of the circle in the
%for loop
pi_est1=zeros(); %pre-loaded with zeros for speed
circ_stdev1=zeros(); %standard deviation array. pre-loaded with zeros for speed
tic
for i=1:numpoint1 %start by using 1 point and work up to the designated amount of points to be generated
    x1=rand(i,1); %'i' row, 1 column vector of random x values will be generated with each iteration of the for loop
    y1=rand(i,1); %same thing for y
    check_in1=(x1-0.5).^2 + (y1-0.5).^2 < 0.5^2; %this is how we'll isolate the points that fell inside of the circle
    pi_est1(i)=sum(check_in1)*4/i; %calculation of pi. sum up the number of points within the circle,
    %multiply by 4, and then divide by the number of points generated
    %during THAT particular iteration
    precision(i)=pi_ref - pi_est1(i); %calculate the precision for each pi
    time(i)=toc;
end
%plot the results
plot(pi_est1,'b'); title('Fig. 1: Estimating Pi with Monte Carlo (Area of a Circle)'); grid on;
plot(precision,'b'); title('Fig. 2:the precision for pi'); grid on;
plot(time,'b'); title('Fig. 3:Times for computing pi'); grid on;
plot(time,precision);xlabel('Time');ylabel('Precision');title('Fig. 4:Precision vs Time');grid on;