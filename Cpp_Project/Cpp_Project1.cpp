#include <iostream>
using namespace std;

//Question 1 Conditional Statements
int main(){
    int x;
    cout <<"enter a number: "<<endl;
    cin >> x;
    if (x == -1){
        cout<<"negative one"<<endl;
    }
    else if (x == 0){
        cout<<"zero"<<endl;
    }
    else if (x == 1){
        cout<<"postivie one"<<endl;
    }
    else {
        cout<<"other value"<<endl;
    }
}

//Question 2 Printing a Vector
#include <vector>
vector<int> myvector(int n) {  // I'd like to find all the factors of 60.
    vector<int> factors;
    int i;
    for (i =1; i <= n; i++){
        if (n % i == 0){
            cout << i << " ";
        }
    }
    return factors;
}

void print_vector(std::vector<int>& v) {
    for (int i = 0; i < v.size(); i++){
        cout << v.at(i) << " ";
    }
    cout << endl;
}

int main(){
   myvector(60);
}

//Question 3 While Loops
int main(){
    int a = 1;
    int b = 2;
    cout << a << "," << b << ","; //output the first two number
    int N = a + b;
    while (a+b <= 4000000){  //use while loop and limit the number whose values do not exceed 4,000,000 
        cout << N << ",";
        a = b;
        b = N;
        N = a + b; //add following numbers
    }
    return N;
}

//Question 4 Functions
//a) If prime
#include <cmath>
bool isprime(int n) {
    int i;
        for (i=2; i < n ; i++)
            if (n % i == 0){      //if there is no remander, it is not s prime number
                return false;
            }
       return true;
}
int main() {
        int n;
        cout << "enter a number: " << endl;
        cin >> n;
        if (isprime(n)){
            cout << n <<" is a prime" << endl;
        }
        else {
            cout << n << " is not a prime" <<endl;
        }
        return 0;
    }
//test cases:
void test_isprime() {
std::cout << "isprime(2) = " << isprime(2) << '\n';
std::cout << "isprime(10) = " << isprime(10) << '\n';
std::cout << "isprime(17) = " << isprime(17) << '\n';
}

//b) Factorize
#include <vector>
std::vector<int> factorize(int n) {
    std::vector<int> answer;
    int i;
        for (i=1; i <= n ; i++)
            if (n % i == 0){    // if there is no remander, it is one of the factor.
                answer.push_back(i);
            }
        return answer;
    return answer;
}
void print_vector(vector<int> const& v) {
    for (int i = 0; i < v.size(); i++) {
        cout << v.at(i) << " ";
    }
    cout << endl;
}
void test_factorize() {
    print_vector(factorize(2));
    print_vector(factorize(72));
    print_vector(factorize(196));
}
int main() {
    test_factorize();
}

//c) Prime Factorization
#include <vector>
std::vector<int> prime_factorize(int n) {
    std::vector<int> answer;
    int i;
        for (i =2; i <= n; i++){    //Since the smallest prime is 2, then I begin with i=2
            while (n % i ==0){
                cout << i << " ";
                n = n/i;
            }
            if (n % i == 0){
                cout << i << endl;
            }
        }
    return answer;
}
void print_vector(vector<int> const& v) {
    for (int i = 0; i < v.size(); i++) {
        cout << v.at(i) << " ";
    }
    cout << endl;
}
void test_prime_factorize() {
    print_vector(prime_factorize(2));
    print_vector(prime_factorize(72));
    print_vector(prime_factorize(196));
}

int main(){
    test_prime_factorize();
}

//Question 5 Recursive Functions and Loops
int main(){
    int n;
    cout << "Number of row: " << endl;
    cin >> n;
    for (int i =0; i <=n; i++){     //In this part, I use two for loops to generate a pascal triangle
        int x = 1;
        for (int j = 0; j <= i; j++){
            cout << x << " ";
            x = x * (i-j)/(j+1);
        }
        cout << endl;
    }
}