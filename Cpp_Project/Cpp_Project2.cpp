#include <iostream>
#include <corecrt_math_defines.h>
#include <iomanip>

double compute_pi(int n) //Define a function to compute pi
{
	double dx = 1.0 / n; //use the definition of integral
	double pi = 0; //set the initial pi as zero
	for (int i = 1; i < n; ++i)//use for loop to compute the integral
	{
		pi += 2 * sqrt(1 - i * dx*i*dx); // use the integral definition to compute pi
	}
	pi += 1;
	pi *= 2 * dx;
	return pi;
}

int main()
{
	std::cout << "Input the number of intervals: "; //  The number of intervals be inputted by the user
	int n = 0;
	std::cin >> n;
	double pi = compute_pi(n);
	std::cout.setf(std::ios::scientific);
	std::cout << "The computing value of pi is " << std::setprecision(10)<<pi << '\n';
	std::cout << "The computing error is " << std::setprecision(10) << abs(pi - m_PI) << '\n';

	std::cout << "Input a tolerance you can accept: ";
	double dtol = 0;
	std::cin >> dtol; 
	int m = (int)sqrt(dtol) / dtol; // let (√dtol / dtol) be the inital estimated number of N, which is denoted as M
	pi = compute_pi(m);
	while (abs(pi - m_PI) > dtol)  // if the computed error is large than the error tolerance, then use while loop to increase the number of m
	{
		m += (int)1/sqrt(dtol); // increase m by 1 / √dtol
		pi = compute_pi(m); // compute the pi again
	}
	std::cout << "The computing error is " << std::setprecision(10) << abs(pi - m_PI) << '\n'; 
	std::cout << "The number of intervals is " << m << '\n'; //print the results
}
